import React from 'react'
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from "react-router-dom";

export default function Layout(props) {
  return (
    <div>
      <Drawer
        // className={classes.drawer}
        variant="permanent"
        // classes={{
        //   paper: classes.drawerPaper,
        // }}
        anchor="left"
      >

        <List>
          <Link to="/">
            <ListItem button>
              <ListItemText>
                {`Home`}
              </ListItemText>
            </ListItem>
          </Link>
          <Link to="/profile">
            <ListItem button>
              <ListItemText>
                {`Profile`}
              </ListItemText>
            </ListItem>
          </Link>
          <a href='#foo'>
            <ListItem button>
              {/* มีpropsเพราะเรียกใช้setIsLoginSuccessจากนอกฟังก์ชัน */}
              <ListItemText onClick={() => props.setIsLoginSuccess(false)}>
                {`Log out`}
              </ListItemText>
            </ListItem>
          </a>

        </List>
        {/* <div className={classes.toolbar} />
        <Divider />
        <List>
          {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          {['All mail', 'Trash', 'Spam'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List> */}
      </Drawer>
      {props.children}
    </div>
  )
}



// const foo = '';
// const bar = '';
// const baz = '';
// ไม่รู้จะตั้งชื่ออะไร