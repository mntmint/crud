import React, { useState } from 'react'
import Paper from '@material-ui/core/paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { update, remove } from 'ramda'
export default function Profile() {

  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [birthday, setBirthday] = useState('')

  const [users, setUsers] = useState([])

  const [index, setIndex] = useState(null)


  console.log(users)
  return (
    <>
      <Paper>
        <TextField label='firstname' value={firstname} onChange={(e) => setFirstname(e.target.value)} />
        <TextField label='lastname' value={lastname} onChange={(e) => setLastname(e.target.value)} />
        <TextField label='birthday'
          type='date'
          InputLabelProps={{ shrink: true }}
          value={birthday} onChange={(e) => setBirthday(e.target.value)} />

        <Button color='primary' variant='contained'
          onClick={() => {
            if (index !== null) {

              setUsers(
                update(index, { firstname, lastname, birthday }, users)
              );
              setIndex(null);
            } else {
              setUsers([...users, { firstname, lastname, birthday }]);


            }
            setFirstname('');
            setLastname('');
            setBirthday('');
          }}
        >{index !== null ? 'Update' : 'Create'}</Button>
      </Paper>

      {
        users.map((user, i) => {
          return (
            <Paper key={i} style={{ marginTop: 10 }}>
              {`firstname: ${user.firstname} , lastname: ${user.lastname} , birthday: ${user.birthday}`}
            </Paper>
          )
        })
      }


      <table align="center">
        <thead>
          <tr>
            <th>No</th>
            <th>firstname</th>
            <th>lastname</th>
            <th>birthday</th>
            <th>edit</th>
            <th>delete</th>

          </tr>
        </thead>
        <tbody>
          {
            users.map((user, i) => {
              return (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{user.firstname}</td>
                  <td>{user.lastname}</td>
                  <td>{user.birthday}</td>
                  <td>
                    <Button color='secondary' variant='outlined'
                      onClick={() => {
                        setFirstname(user.firstname);
                        setLastname(user.lastname);
                        setBirthday(user.birthday);
                        setIndex(i);
                      }}>{`Edit`}</Button>
                  </td>
                  <td>
                    <Button color='secondary' variant='outlined'
                      onClick={() => {
                        if (window.confirm('Cofirm')) {
                          setUsers(remove(i, 1, users))
                          alert('ลบสำเร็จ')
                        }
                      }}>{`Delete`}</Button>
                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>

    </>
  )
}
