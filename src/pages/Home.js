import React from 'react'

export default function Home() {

  const arr = new Array(100).fill(null);


  return (
    <div>
      {
        arr.map((each, i) => {

          const num = i + 1
          if (num % 3 !== 0 && num % 5 !== 0) {
            return null;
          }

          return <div key={num}>{`${num} ${num % 3 === 0 ? 'Foo' : ''} ${num % 5 === 0 ? 'Bar' : ''}`} </div>
          // return <div key={num}>{`${num % 3 === 0 || num % 5 === 0 ? num : ''} ${num % 3 === 0 ? 'Foo' : ''} ${num % 5 === 0 ? 'Bar' : ''}`} </div>


        }
        )}
    </div>
  )
}
