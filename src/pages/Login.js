import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import './Login.css'



export default function Login(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  return (
    <div className="login-page">
      <Paper className="wrapper">
        <TextField
          label="Username"
          margin='dense'
          value={username}
          onChange={(e) => setUsername(e.target.value)} />
        <TextField
          label="Password"
          type='password'
          margin='dense'
          value={password}
          onChange={(e) => setPassword(e.target.value)} />

        <Button
          onClick={() => {
            props.onLogin(username, password);
          }
          }
          variant="contained"
          color="primary"
          style={{ marginTop: 10 }}>
          {'LOGIN'}
        </Button>
      </Paper>
    </div>
  )
}
