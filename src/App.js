import React, { useState } from 'react';
import './App.css';
import { Router, Route, Switch, Redirect } from "react-router";
import { createBrowserHistory } from "history";

import Login from './pages/Login'
import Home from './pages/Home'
import Profile from './pages/Profile'
import Layout from './pages/Layout';

const history = createBrowserHistory();


function App() {

  const [isLoginSuccess, setIsLoginSuccess] = useState(true)

  const onLogin = (username, password) => {
    if (username === 't' && password === 't') {
      setIsLoginSuccess(true);
      history.push('/');
    }
    else setIsLoginSuccess(false);
  }

  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <PrivateRoute exact path="/" isLoginSuccess={isLoginSuccess} setIsLoginSuccess={setIsLoginSuccess}>
            <Home />
          </PrivateRoute>
          {/* เช็คค่าlogin เพื่อจะได้รู้ว่าจะส่งไปหน้าไหน */}
          <PrivateRoute exact path="/profile" isLoginSuccess={isLoginSuccess} setIsLoginSuccess={setIsLoginSuccess}>
            <Profile />
          </PrivateRoute>
          <Route path="/login" render={() => isLoginSuccess ? <Redirect to="/" /> : <Login onLogin={onLogin} />} />

          <Redirect to="/" />
        </Switch>
      </Router>
    </div>
  );
}

export default App;


function PrivateRoute({ children, isLoginSuccess, setIsLoginSuccess, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isLoginSuccess ? (
          // ส่งpropsเพื่อไปใช้งานในlayout
          <Layout setIsLoginSuccess={setIsLoginSuccess}>

            {children}
          </Layout>

        ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
      }
    />
  );
}